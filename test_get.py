import unittest
import main


class TestGet(unittest.TestCase):

    def setUp(self):
        main.app.testing = True
        self.main = main.app.test_client()

    def test_get_5(self):
        rv = self.main.get('/get5/')
        self.assertEqual(rv.status, '200 OK')
        self.assertEqual(rv.data, b'5')

    def test_get_15(self):
        rv = self.main.get('/get15/')
        self.assertEqual(rv.status, '200 OK')
        self.assertEqual(rv.data, b'15')


if __name__ == '__main__':
    import xmlrunner
    runner = xmlrunner.XMLTestRunner(output='test-reports')
    unittest.main(testRunner=runner)
    unittest.main()


