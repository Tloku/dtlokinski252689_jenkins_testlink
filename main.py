from flask import Flask
app = Flask(__name__)


@app.route('/')
@app.route('/get5/')
def get_5():
    return '5'


@app.route('/get15/')
def get_15():
    return '15'


if __name__ == '__main__':
    app.run(host='0.0.0.0')

